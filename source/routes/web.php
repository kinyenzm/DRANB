<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

Route::resource('app', 'CustomerController');
Route::resource('client', 'ClientController');
Route::resource('merchant', 'MerchantController');
Route::resource('category', 'CategoryController');
Route::resource('brand', 'StoreController');
Route::resource('product', 'ProductController');
Route::resource('bag', 'BagController');
Route::resource('feedback', 'FeedbackController');

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_brands', function (Blueprint $table) {
            $table->unsignedInteger('id_payment_brand');
            // $table->tinyInteger('cash');
            // $table->tinyInteger('paypal');
            // $table->tinyInteger('payU');
            $table->string('method_of_payment_brand');
            $table->primary('id_payment_brand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_brands');
    }
}

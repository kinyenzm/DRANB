<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('reference')->unsigned();
            $table->string('productName', 45);
            $table->mediumText('short_description');
            $table->decimal('price', 9, 2);
            $table->string('product_image')->nullable();
            // once the table is created use a raw query to ALTER it and add the LONGBLOB
            // DB::statement("ALTER TABLE product MODIFY product_image LONGBLOB NULL");
            $table->string('brand_name', 45);
            $table->integer('product_category')->unsigned();
            $table->integer('id_payment_product')->unsigned();
            $table->integer('id_delivery_product')->unsigned();
            $table->foreign('brand_name')->references('brand_name')->on('brands');
            $table->foreign('product_category')->references('id_category')->on('categories');
            $table->foreign('id_payment_product')->references('id_payment_product')->on('payment_products');
            $table->foreign('id_delivery_product')->references('id_delivery_product')->on('delivery_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('user_email', 99)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('nickname', 45);
            $table->string('password', 128);
            $table->string('name', 81);
            $table->string('lastname', 81)->nullable();
            $table->string('photo')->nullable();
            // once the table is created use a raw query to ALTER it and add the LONGBLOB
            // DB::statement("ALTER TABLE user MODIFY photo LONGBLOB NULL");
            $table->string('address', 150)->nullable();
            $table->string('phone')->nullable();
            $table->primary('user_email');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

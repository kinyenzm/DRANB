<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBagFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bag_factures', function (Blueprint $table) {
            $table->increments('id_bag_facture')->unsigned();
            $table->integer('customer')->unsigned();
            $table->decimal('tax', 9, 2);
            $table->decimal('total', 9, 2);
            $table->foreign('customer')->references('id_customer')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bag_factures');
    }
}

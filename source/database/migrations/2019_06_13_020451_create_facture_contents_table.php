<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactureContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_contents', function (Blueprint $table) {
            $table->integer('bag_facture')->unsigned();
            $table->integer('product_Reference')->unsigned();
            $table->decimal('amount', 9, 2);
            $table->decimal('subtotal', 9, 2);
            $table->primary(['bag_facture', 'product_Reference']);
            $table->foreign('product_Reference')->references('reference')->on('products');
            $table->foreign('bag_facture')->references('id_bag_facture')->on('bag_factures');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_contents');
    }
}

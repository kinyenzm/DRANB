<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->string('brand_name', 45);
            $table->string('speciality', 45);
            $table->mediumText('description');
            $table->integer('deliveryBrand')->unsigned();
            $table->integer('paymentBrand')->unsigned();
            $table->integer('merchant')->unsigned();
            $table->string('logo')->nullable();
            // once the table is created use a raw query to ALTER it and add the LONGBLOB
            // DB::statement("ALTER TABLE brand MODIFY logo LONGBLOB NULL");
            $table->string('socialmedia_facebook', 45)->nullable();
            $table->string('socialmedia_twitter', 45)->nullable();
            $table->string('socialmedia_instagram', 45)->nullable();
            $table->string('contact_brand', 45)->nullable();
            $table->primary('brand_name');
            $table->foreign('deliveryBrand')->references('id_delivery_brand')->on('delivery_brands');
            $table->foreign('paymentBrand')->references('id_payment_brand')->on('payment_brands');
            $table->foreign('merchant')->references('id_merchant')->on('merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}

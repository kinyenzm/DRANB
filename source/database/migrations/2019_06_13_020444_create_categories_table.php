<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id_category')->unsigned();
            $table->integer('subCategory')->unsigned()->nullable();
            $table->mediumText('short_description');
            $table->string('category_logo')->nullable();
            // once the table is created use a raw query to ALTER it and add the LONGBLOB
            // DB::statement("ALTER TABLE category MODIFY category_logo LONGBLOB NULL");
            $table->foreign('subCategory')->references('id_category')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

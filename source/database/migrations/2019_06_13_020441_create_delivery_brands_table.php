<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_brands', function (Blueprint $table) {
            $table->unsignedInteger('id_delivery_brand');
            // $table->tinyInteger('hand_to_hand');
            // $table->tinyInteger('land_transport');
            // $table->tinyInteger('air_transport');
            $table->string('method_of_delivery_brand');
            $table->primary('id_delivery_brand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_brands');
    }
}

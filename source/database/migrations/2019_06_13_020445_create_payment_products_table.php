<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_products', function (Blueprint $table) {
            $table->increments('id_payment_product')->unsigned();
            // $table->tinyInteger('cash');
            // $table->tinyInteger('paypal');
            // $table->tinyInteger('payU');
            $table->string('method_of_payment_product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_products');
    }
}

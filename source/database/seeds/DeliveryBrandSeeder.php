<?php

use Illuminate\Database\Seeder;

class DeliveryBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('delivery_brands')->truncate();
        factory(App\DeliveryBrand::class, 5)->create();
    }
}

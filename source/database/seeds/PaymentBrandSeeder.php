<?php

use Illuminate\Database\Seeder;

class PaymentBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_brands')->truncate();
        factory(App\PaymentBrand::class, 5)->create();
    }
}

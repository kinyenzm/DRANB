<?php

use Illuminate\Database\Seeder;

class DeliveryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('delivery_products')->truncate();
        factory(App\DeliveryProduct::class, 5)->create();
    }
}

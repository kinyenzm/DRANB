<?php

use Illuminate\Database\Seeder;

class BagFactureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bag_factures')->truncate();
        factory(App\BagFacture::class, 3)->create();
    }
}

<?php

use Illuminate\Database\Seeder;

class FactureContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facture_contents')->truncate();
        factory(App\FactureContent::class, 3)->create();
    }
}

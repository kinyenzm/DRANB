<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Eloquent::unguard();
        //desactiva el chequeo de las llaves foraneas de esta conexion para correr los seeder's
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(MerchantSeeder::class);
        $this->call(FeedBackSeeder::class);
        $this->call(DeliveryBrandSeeder::class);
        $this->call(PaymentBrandSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(PaymentProductSeeder::class);
        $this->call(DeliveryProductSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(BagFactureSeeder::class);
        $this->call(FactureContentSeeder::class);
        // Luego de estar llenadas las tablas se procede a activar el chequeo

        // La siguiente linea activa el chequeo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

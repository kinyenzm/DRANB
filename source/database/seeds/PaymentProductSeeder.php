<?php

use Illuminate\Database\Seeder;

class PaymentProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_products')->truncate();

        factory(App\PaymentProduct::class, 5)->create();
    }
}

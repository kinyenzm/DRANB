<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DeliveryBrand;
use Faker\Generator as Faker;

$factory->define(DeliveryBrand::class, function (Faker $faker) {
    return [
        'id_delivery_brand' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'method_of_delivery_brand' => $faker->randomElement(['hand to hand', 'land transport', 'air transport'])
    ];
});

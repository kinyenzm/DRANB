<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'id_category' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'subCategory' => $faker->randomElement(\App\Category::pluck('id_category')),
        'short_description' => $faker->sentence($nbWords = 2, $variableNbWords = true),
        'category_logo' => $faker->company
    ];
});

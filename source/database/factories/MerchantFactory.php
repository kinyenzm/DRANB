<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Merchant;
use Faker\Generator as Faker;

$factory->define(Merchant::class, function (Faker $faker) {
    return [
        'id_merchant' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'user_email' => $faker->randomElement(\App\User::pluck('user_email')),
        'user_role' => $faker->randomElement(\App\Role::pluck('id_role'))
    ];
});

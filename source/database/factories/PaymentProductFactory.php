<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PaymentProduct;
use Faker\Generator as Faker;

$factory->define(PaymentProduct::class, function (Faker $faker) {
    return [
        'id_payment_product' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'method_of_payment_product' => $faker->randomElement(['cash', 'paypal', 'payU'])
    ];
});

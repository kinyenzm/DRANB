<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BagFacture;
use Faker\Generator as Faker;

$factory->define(BagFacture::class, function (Faker $faker) {
    return [
        'id_bag_facture' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'Customer' => $faker->randomElement(\App\Customer::pluck('id_customer')),
        'tax' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 9999),
        'total' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 9999)
    ];
});

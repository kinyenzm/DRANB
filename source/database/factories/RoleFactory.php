<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'id_role' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'role' => $faker->randomElement(['comprador', 'vendedor', 'administrador', 'asesor'])
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\FactureContent;
use Faker\Generator as Faker;

$factory->define(FactureContent::class, function (Faker $faker) {
    return [
        'bag_facture' => $faker->randomElement(\App\BagFacture::pluck('id_bag_facture')),
        'product_Reference' => $faker->randomElement(\App\Product::pluck('reference')),
        'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 9999),
        'subtotal' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 9999)
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PaymentBrand;
use Faker\Generator as Faker;

$factory->define(PaymentBrand::class, function (Faker $faker) {
    return [
        'id_payment_brand' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'method_of_payment_brand' => $faker->randomElement(['cash', 'paypal', 'payU'])
    ];
});

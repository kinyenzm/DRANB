<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'user_email' => $faker->unique()->freeEmail,
        'email_verified_at' => now(),
        'nickname' => $faker->unique()->userName,
        'password' => $faker->unique()->password,
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'photo' => $faker->mimeType,
        'address' => $faker->streetAddress,
        'phone' => $faker->unique()->phoneNumber,
        'remember_token' => Str::random(10),
    ];
});

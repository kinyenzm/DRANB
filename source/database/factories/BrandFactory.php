<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Brand;
use Faker\Generator as Faker;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'brand_name' => $faker->unique()->name,
        'speciality' => $faker->company,
        'description' => $faker->company,
        'deliveryBrand' => $faker->randomElement(\App\DeliveryBrand::pluck('id_delivery_brand')),
        'paymentBrand' => $faker->randomElement(\App\PaymentBrand::pluck('id_payment_brand')),
        'merchant' => $faker->randomElement(\App\Merchant::pluck('id_merchant')),
        'logo' => $faker->fileExtension,
        'socialmedia_facebook' => $faker->fileExtension,
        'socialmedia_twitter' => $faker->fileExtension,
        'socialmedia_instagram' => $faker->fileExtension,
        'contact_brand' => $faker->company
    ];
});

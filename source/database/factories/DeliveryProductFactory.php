<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DeliveryProduct;
use Faker\Generator as Faker;

$factory->define(DeliveryProduct::class, function (Faker $faker) {
    return [
        'id_delivery_product' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'method_of_delivery_product' => $faker->randomElement(['hand to hand', 'land transport', 'air transport'])
    ];
});

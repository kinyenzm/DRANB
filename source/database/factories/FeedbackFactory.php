<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Feedback;
use Faker\Generator as Faker;

$factory->define(Feedback::class, function (Faker $faker) {
    return [
        'num_error' => $faker->unique()->numberBetween($min = 1000, $max = 9000),
        'user_email' => $faker->randomElement(\App\User::pluck('user_email')),
        'description' => $faker->text,
        'type_report' => $faker->name
    ];
});

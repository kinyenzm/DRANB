<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'reference' => $faker->unique()->numberBetween($min = 0000, $max = 9000),
        'productName' => $faker->company,
        'short_description' => $faker->company,
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 9999),
        'product_image' => $faker->fileExtension,
        'brand_name' => $faker->randomElement(\App\Brand::pluck('brand_name')),
        'product_category' => $faker->randomElement(\App\Category::pluck('id_category')),
        'id_payment_product' => $faker->randomElement(\App\PaymentProduct::pluck('id_payment_product')),
        'id_delivery_product' => $faker->randomElement(\App\DeliveryProduct::pluck('id_delivery_product'))
    ];
});

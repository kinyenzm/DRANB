<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactureContent extends Model
{
    protected $table = 'facture_contents';
    protected $primaryKey = ['bag_facture'];
    protected $fillable = ['product_Reference','amount', 'subtotal'];
    public $incrementing = false;

    public function products()
    {
        return $this->hasMany('products', 'reference');
    }

    public function bagFactures()
    {
        return $this->hasMany('bag_factures', 'id_bag_facture');
    }
}

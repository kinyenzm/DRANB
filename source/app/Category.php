<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'id_category';
    protected $fillable = ['subCategory', 'short_description', 'category_logo'];
    public $incrementing = false;

    public function categories()
    {
        return $this->hasMany('categories', 'id_category');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryBrand extends Model
{
    protected $table = 'delivery_brands';
    protected $primaryKey = 'id_delivery_brand';
    protected $fillable = ['method_of_delivery_brand'];
    public $incrementing = false;
}

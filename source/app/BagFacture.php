<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BagFacture extends Model
{
    protected $table = 'bag_factures';
    protected $primaryKey = 'id_bag_facture';
    protected $fillable = ['customer', 'tax', 'total'];
    public $incrementing = false;

    public function customers()
    {
        return $this->hasMany('customers', 'id_customer');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentProduct extends Model
{
    protected $table = 'payment_products';
    protected $primaryKey = 'id_payment_product';
    protected $fillable = ['method_of_payment_product'];
    public $incrementing = false;
}

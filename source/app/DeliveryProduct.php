<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryProduct extends Model
{
    protected $table = 'delivery_products';
    protected $primaryKey = 'id_delivery_product';
    protected $fillable = ['method_of_delivery_product'];
    public $incrementing = false;
}

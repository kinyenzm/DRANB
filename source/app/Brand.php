<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $primaryKey = 'brand_name';
    protected $fillable = ['speciality', 'description', 'deliveryBrand', 'paymentBrand', 'merchant', 'logo', 'socialmedia_facebook', 'socialmedia_twitter', 'socialmedia_instagram', 'contact_brand'];
    public $incrementing = false;

    public function merchants()
    {
        return $this->hasMany('merchants', 'id_merchant');
    }

    public function payments()
    {
        return $this->hasMany('payment_brands', 'id_payment_brand');
    }

    public function deliveries()
    {
        return $this->hasMany('delivery_brands', 'id_delivery_brand');
    }
}

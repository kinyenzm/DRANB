<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $primaryKey = 'id_customer';
    protected $fillable = ['user_email', 'user_role'];
    public $incrementing = false;

    public function users()
    {
        return $this->hasMany('users', 'user_email');
    }

    public function roles()
    {
        return $this->hasMany('roles', 'id_role');
    }
}

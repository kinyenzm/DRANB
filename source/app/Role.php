<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id_role';
    protected $fillable = ['role'];
    public $incrementing = false;
    
    public function merchants()
    {
        return $this->hasMany('merchants', 'id_merchant');
    }

    public function customers()
    {
        return $this->hasMany('customers', 'id_customer');
    }
}

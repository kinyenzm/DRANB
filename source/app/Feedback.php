<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';
    protected $primaryKey = 'num_error';
    protected $fillable = ['user_email', 'description', 'type_report'];
    public $incrementing = false;

    public function users()
    {
        return $this->hasMany('users', 'user_email');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentBrand extends Model
{
    protected $table = 'payment_brands';
    protected $primaryKey = 'id_payment_brand';
    protected $fillable = ['method_of_payment_brand'];
    public $incrementing = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'reference';
    protected $fillable = ['productName', 'short_description', 'price', 'product_image', 'brand_name', 'product_category', 'id_payment_product', 'id_delivery_product'];
    public $incrementing = false;

    public function brands()
    {
        return $this->hasMany('brands', 'brand_name');
    }

    public function payments()
    {
        return $this->hasMany('payment_products', 'id_payment_product');
    }

    public function deliveries()
    {
        return $this->hasMany('delivery_products', 'id_delivery_product');
    }

    public function categories()
    {
        return $this->hasMany('categories', 'id_category');
    }
}

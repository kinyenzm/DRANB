@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="css/style.css">
@endsection
@section('titulo')
DATOS DE VENDEDOR
@endsection
@section('contenido')
<!-- <header>
    <nav class="navbar-dark bg-dark px-4">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-brand">Brand</a>
            </li>
            <li class="nav-items">
                <div class="btn-group dropleft">
                    <button type="button" class="btn btn-dark">Usuario <i class="far fa-user-circle"></i></button>
                    <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Usuario</span>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Editar Perfil</a>
                        <a class="dropdown-item" href="#">Carrito</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Cerrar sesión</a>
                    </div>
                </div>
            </li>
            <li class="nav-items">
                <a href="#" class="nav-link"><i class="fas fa-question-circle"></i></a>
            </li>
        </ul>
    </nav>
</header> -->
<header>
    <nav class="navbar-dark bg-dark px-4">
        <ul class="navbar-nav flex-row justify-content-between">
            <li class="nav-item">
                <a href="/brand/show" class="navbar-brand">Brand</a>
            </li>
            <li class="nav-items">
                <a href="{{ route('feedback.index')}}" class="nav-link"><i class="fas fa-question-circle"></i></a>
            </li>
        </ul>
    </nav>
</header>
<main class="container-fluid">
    <div class="row justify-content-around">
        <section class="col-2">
            <h2>Editar Marca</h2>
            <div>
                <h5>Datos vendedor</h5>
                <hr>
                <p><a href="#dc">Datos de Cuenta</a></p>
                <p><a href="#dp">Datos Personales</a></p>
                <p><a href="#de">Datos de Localización</a></p>
            </div>
        </section>
        <section class="col-7">
            <h1 class="text-center mb-5">Datos de vendedor</h1>
            <div class="data" id="dc">
                <h2 class="m-2 text-center">Datos de cuenta</h2>
                <hr class="w-100">
                <div class="form-group row">
                    <label for="usr" class="col-4 col-form-label text-right"><b>Usuario: </b></label>
                    <div class="col-6">
                        <input type="text" class="form-control" name="usr" id="usr" placeholder="Pepito0054">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mail" class="col-4 col-form-label text-right"><b>E-mail: </b></label>
                    <div class="col-6">
                        <input type="email" class="form-control" name="mail" id="mail" placeholder="ejemplo@dominio.com">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pass" class="col-4 col-form-label text-right"><b>Clave: </b></label>
                    <div class="col-6">
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="6 - 20 caracteres [letras y números]">
                    </div>
                </div>
                <div class="row justify-content-end">
                    <a href="#"><i class="fas fa-check"></i></a>
                    <a href="#"><i class="fas fa-times"></i></a>
                </div>
            </div>
            <div class="data" id="dp">
                <h2 class="m-2 text-center">Datos Personales</h2>
                <hr class="w-100">
                <div class="form-group row">
                    <label for="fullname" class="col-4 col-form-label text-right"><b>Nombre y apellido: </b></label>
                    <div class="col-6">
                        <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Pepito Alzate">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tel" class="col-4 col-form-label text-right"><b>Teléfono: </b></label>
                    <div class="col-6">
                        <input type="number" class="form-control" name="tel" id="tel" placeholder="(1) 777***7">
                    </div>
                </div>
                <div class="row justify-content-end">
                    <a href="#"><i class="fas fa-check"></i></a>
                    <a href="#"><i class="fas fa-times"></i></a>
                </div>
            </div>
            <div class="data" id="de">
                <h2 class="m-2 text-center">Datos de Localización</h2>
                <hr class="w-100">
                <div class="form-group row">
                    <label for="usr" class="col-4 col-form-label text-right"><b>Dirección: </b></label>
                    <div class="col-6">
                        <input type="text" class="form-control" name="usr" id="usr" placeholder="Av. Calle.">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mail" class="col-4 col-form-label text-right"><b>Lugar: </b></label>
                    <div class="col-6">
                        <input type="email" class="form-control" name="mail" id="mail" placeholder="Apartamento Casa Conjunto">
                    </div>
                </div>
                <div class="row justify-content-end">
                    <a href="#"><i class="fas fa-check"></i></a>
                    <a href="#"><i class="fas fa-times"></i></a>
                </div>
            </div>
        </section>
        <aside class="col-3">
            <h2 class="text-center mb-5">Imagen Marca</h2>
            <div class="d-flex justify-content-center">
                <div class="dropdown">
                    <a class="btn btn-secondary" href="#" role="button" id="dropdown-image-porfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown-image-porfile">
                        <a class="dropdown-item" href="#">Editar Imagen</a>
                        <a class="dropdown-item" href="#">Eliminar Imagen</a>
                    </div>
                </div>
                <div class="image-perfile">
                    <img src="http://barcarena.pa.gov.br/portal/img/perfil/padrao.jpg" alt="image">
                </div>
            </div>
        </aside>
    </div>
</main>
<footer class="w-100 bg-dark text-white-50">
    <div class="text-monospace text-center">
        <small>Derechos Reservados &copy; 2019 DRANB</small>
    </div>
</footer>
@endsection
@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="/css/style.css">
@endsection
@section('titulo')
Comentarios
@endsection
@section('contenido')
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="/brand/show">Brand</a>
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" role="button" aria-expanded="false">
                            INICIO
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            PRODUCTOS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('product.index') }}">PRODUCTO ALEATORIO #1</a>
                            <a class="dropdown-item" href="{{ route('product.index') }}">PRODUCTO ALEATORIO #2</a>
                            <a class="dropdown-item" href="{{ route('product.index') }}">PRODUCTO ALEATORIO #3</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            CATEGORIAS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('category.index') }}">HOGAR</a>
                            <a class="dropdown-item" href="{{ route('category.index') }}">CUIDADO PERSONAL</a>
                            <a class="dropdown-item" href="{{ route('category.index') }}">DECORACIONES</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MARCAS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">JARDINERIA - JUAN</a>
                            <a class="dropdown-item" href="#">FRUVER - CARLOS</a>
                            <a class="dropdown-item" href="#">DROGUERIAS - JOSE</a>
                        </div>
                    </li>
                </ul>
            </div>
            <h6 class="text-white-50 ml-3">Mi Bolsa</h6>
            <a href="{{ route('bag.index') }}"><i class="fas fa-shopping-bag mx-3" style="font-size: 32px; color: #212529;"></i></a>
            <a href="{{ route('client.index') }}"><i class="fas fa-user" style="font-size: 32px; color: #212529;"></i></a>
        </div>
    </nav>
</header>
<main>
    <h1 class="text-center my-5">Carrito</h1>
</main>
<footer>
    <div class="row">
        <div class="col-md-4" style="background-color: #DAE1DF">
            <center>
                <h3 class="m-3">Informacion sobre tu vendedor</h3>
                <p class="m-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </center>
        </div>
        <div class="col-md-4" style="background-color: #5C5C5C">
            <center>
                <h3 class="m-3">Servicios</h3>
                <p class="m-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </center>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                    <i class="fas fa-car" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="fas fa-american-sign-language-interpreting" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="far fa-money-bill-alt" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="fab fa-cc-paypal" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
        <div class="col-md-4" style="background-color: #959595; padding: 1rem;">
            <h3 class="text-center mb-5">Contactenos</h3>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <i class="fab fa-facebook-square" style="font-size: 50px"></i>
                        </div>
                        <div class="col-md-4">
                            <i class="fab fa-instagram" style="font-size: 50px"></i>
                        </div>
                        <div class="col-md-4">
                            <i class="fab fa-twitter" style="font-size: 50px"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-10">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>
                <div class="col-md-1">
                </div>
            </div>
        </div>
    </div>
</footer>
@endsection
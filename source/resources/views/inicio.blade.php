@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="/css/estilos.css">
@endsection
@section('titulo')
INICIO
@endsection
@section('contenido')
<main>
    <div class="container-fluid main">
        <div class="row">
            <div class="col-sm-7">
                <a href="{{ route('app.index') }}"><img src="images/logo.png"></a>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="row justify-content-center">
                    <p class="m-3 text-white" style="margin-top:200px">Acá selecciona una tienda -></p>
                </div>
            </div>
            <div class="col-sm-5">
                <a href="{{ route('brand.index') }}"><img src="images/ruleta.png" style="margin-top: 180px" class="img-fluid"></a>
            </div>
        </div>
    </div>
</main>
<footer class="w-100 bg-dark text-white-50">
    <div class="text-monospace text-center">
        <small>Derechos Reservados &copy; 2019 <a href="/">Dranb</a> Tecnologies</small>
    </div>
</footer>
@endsection
@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="/css/estilos.css">
@endsection
@section('titulo')
MARCA | EDITAR
@endsection
@section('contenido')
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <!-- <span class="navbar-toggler-icon"></span> -->
        <a class="navbar-brand" href="{{ route('brand.index') }}">Brand</a>
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" role="button" aria-expanded="false">
                            INICIO
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            PRODUCTOS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/product/show">PRODUCTO ALEATORIO #1</a>
                            <a class="dropdown-item" href="/product/show">PRODUCTO ALEATORIO #2</a>
                            <a class="dropdown-item" href="/product/show">PRODUCTO ALEATORIO #3</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            CATEGORIAS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/category/show">HOGAR</a>
                            <a class="dropdown-item" href="/category/show">CUIDADO PERSONAL</a>
                            <a class="dropdown-item" href="/category/show">DECORACIONES</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MARCAS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">JARDINERIA - JUAN</a>
                            <a class="dropdown-item" href="#">FRUVER - CARLOS</a>
                            <a class="dropdown-item" href="#">DROGUERIAS - JOSE</a>
                        </div>
                    </li>
                </ul>
            </div>
            <h6 class="text-white-50 ml-3">Mi Bolsa</h6>
            <a href="{{ route('bag.index') }}"><i class="fas fa-shopping-bag mx-3" style="font-size: 32px; color: #212529;"></i></a>
            <a href="{{ route('merchant.index') }}"><i class="fas fa-user" style="font-size: 32px; color: #212529;"></i></a>
        </div>
    </nav>
</header>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
                <center><img src="/images/ultima.png" width="100%"></center>
            </div>
            <div class="col-md-4">
                <center><img src="/images/ultima2.png" width="100%"></center>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
</main>
<footer>
    <div class="row">
        <div class="col-md-4 p-3" style="background-color: #DAE1DF">
            <center>
                <h3 class="text-center mb-3">Informacion sobre tu vendedor</h3>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </center>
        </div>
        <div class="col-md-4 p-3" style="background-color: #5C5C5C">
            <h3 class="text-center mb-3">Servicios</h3>
            <textarea class="form-control" rows="3"></textarea>
            <div class="row mt-1 p-3">
                <div class="col-md-2">
                    <i class="fas fa-rocket" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="fas fa-car" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="fas fa-american-sign-language-interpreting" style="font-size: 50px"></i>

                </div>
                <div class="col-md-2">
                    <i class="far fa-money-bill-alt" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="fas fa-credit-card" style="font-size: 50px"></i>
                </div>
                <div class="col-md-2">
                    <i class="fab fa-cc-paypal" style="font-size: 50px"></i>
                </div>
            </div>
        </div>
        <div class="col-md-4 p-3" style="background-color: #959595">
            <h3 class="text-center mb-3">Contactenos</h3>
            <div class="row m-3">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <i class="fab fa-facebook-square" style="font-size: 50px"></i>
                        </div>
                        <div class="col-md-4">
                            <i class="fab fa-instagram" style="font-size: 50px"></i>
                        </div>
                        <div class="col-md-4">
                            <i class="fab fa-twitter" style="font-size: 50px"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-10">
                    <textarea class="form-control" rows="3"></textarea>
                </div>
                <div class="col-md-1">
                </div>
            </div>
        </div>
    </div>
</footer>
@endsection
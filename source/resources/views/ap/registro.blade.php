@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="/css/style.css">
@endsection
@section('titulo')
REGISTRATE
@endsection
@section('contenido')
<header>
    <nav class="navbar-dark bg-dark px-4">
        <ul class=" navbar-nav flex-row justify-content-between">
            <li class="nav-item">
                <a href="/" class="navbar-brand">Dranb</a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <h2 class="text-center">¡Unete!</h2>
    <form action="" method="POST">
        <section class="d-flex justify-content-center align-items-center" id="reg">
            <div class="registro">
                <div class="form-group row">
                    <label for="mail" class="col col-form-label">Correo electronico</label>
                    <div class="col-8">
                        <input type="email" class="form-control" name="mail" id="mail" placeholder="ejemplo@dominio.com" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cmail" class="col col-form-label">Confirmar Correo electronico</label>
                    <div class="col-8">
                        <input type="email" class="form-control" name="cmail" id="cmail" placeholder="Reingresar ejemplo@dominio.com" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="firstname" class="col col-form-label">Nombre</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Nombre" required>
                    </div>
                    <div class="col-4">
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Apellidos" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pass" class="col col-form-label">Contraseña</label>
                    <div class="col-8">
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="6 - 20 caracteres [letras y números]" required>
                    </div>
                </div>
                <p class="text-center"><i>Hola, persona interserada en servicios Dranb es para nosotros de gran
                        alegria
                        que una persona quiera confiar y particiar en lo propuesto por nosotros como organizacion
                        entonces que esperas unete y comienza a buscar lo que algunos provedores locales tienen para
                        ofrecerte, ¡comienza ahora!</i></p>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="terms" required>
                    <label class="custom-control-label" for="terms">Click para aceptar <a href="#" target="_blank" rel="noopener noreferrer">Terminos y condiciones</a></label>
                </div>
            </div>
        </section>
        <div class="row justify-content-center">
            <button type="submit" class="mt-3 btn btn-warning">Registrame</button>
        </div>
        <div class="row justify-content-center">
            <p>Si ya tienes cuenta <a href="{{ route('app.index') }}">inicia sesión</a>.</p>
        </div>
    </form>
</main>
<footer class="w-100 bg-dark text-white-50 fixed-bottom">
    <div class="text-monospace text-center">
        <small>Derechos Reservados &copy; 2019 <a href="/">Dranb</a> Tecnologies</small>
    </div>
</footer>
@endsection
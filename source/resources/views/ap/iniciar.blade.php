@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="/css/style.css">
@endsection
@section('titulo')
INICIA SESIÓN
@endsection
@section('contenido')
<header>
    <nav class="navbar-dark bg-dark px-4">
        <ul class=" navbar-nav flex-row justify-content-between">
            <li class="nav-item">
                <a href="/" class="navbar-brand">Dranb</a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <h2 class="m-5 text-center">¡Hola!</h2>
    <form action="perfil_cliente.html" method="POST">
        <section class="d-flex justify-content-center align-items-center" id="start">
            <div class="inicio">
                <h5 class="p-2 text-center">Ingresa tus datos</h5>
                <div class="form-group">
                    <input type="email" class="form-control" name="mail" id="mail" placeholder="Usuario / E-mail" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="pass" id="pass" placeholder="Contraseña" required>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="terms" required>
                    <label class="custom-control-label" for="terms">Recuérdame <a href="#">¿Olvidaste tu contraseña?</a></label>
                </div>
            </div>
        </section>
        <div class="row justify-content-center">
            <button type="submit" class="mt-3 btn btn-secondary">Ingresar</button>
        </div>
        <div class="row justify-content-center">
            <p>Si no tines cuenta aún unete <a href="/app/show">click acá</a>.</p>
        </div>
    </form>
</main>
<footer class="w-100 bg-dark text-white-50 fixed-bottom">
    <div class="text-monospace text-center">
        <small>Derechos Reservados &copy; 2019 <a href="/">Dranb</a> Tecnologies</small>
    </div>
</footer>
@endsection
@extends('layout.layout')
@section('estilo')
<link rel="stylesheet" href="/css/style.css">
@endsection
@section('titulo')
Comentarios
@endsection
@section('contenido')
<header>
    <nav class="navbar-dark bg-dark px-4">
        <ul class=" navbar-nav flex-row justify-content-between">
            <li class="nav-item">
                <a href="/" class="navbar-brand">Brand</a>
            </li>
            <li class="nav-items">
                <a href="{{ route('brand.index')}}" class="nav-link"><i class="fas fa-user"></i></a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <h2 class="m-5 text-center">¡Hola!</h2>
    <form action="perfil_cliente.html" method="POST">
        <section class="d-flex justify-content-center align-items-center m-1 p-1">
            <div class="comentarios">
                <h5>Detallanos tu problema</h5>
            </div>
        </section>

        <section class="d-flex justify-content-center align-items-center" id="start">
            <div>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Tu nombre" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="mail" id="mail" placeholder="Correo electronico" required>
                </div>
                <div class="form-group">
                    <select class="form-control" id="exampleFormControlSelect1" required>
                        <option value="" selected>Tipo de problema:</option>
                        <option>Compras</option>
                        <option>Vendedor</option>
                        <option>Sistema</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Describe tu caso" rows="3" required></textarea>
                </div>
            </div>
        </section>
        <div class="row justify-content-center">
            <button type="submit" class="mt-3 btn btn-outline-secondary">Enviar</button>
        </div>
    </form>
</main>
<footer class="w-100 bg-dark text-white-50 fixed-bottom">
    <div class="text-monospace text-center">
        <small>Derechos Reservados &copy; 2019 <a href="/">Dranb</a> Tecnologies</small>
    </div>
</footer>
@endsection
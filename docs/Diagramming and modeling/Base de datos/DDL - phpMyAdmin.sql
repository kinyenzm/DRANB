﻿-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-05-2018 a las 19:34:00
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_dranb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bag_facture`
--

CREATE TABLE `bag_facture` (
  `idBag_facture` int(10) UNSIGNED NOT NULL,
  `Customer` int(10) UNSIGNED NOT NULL,
  `tax` decimal(9,2) NOT NULL,
  `total` decimal(9,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brand`
--

CREATE TABLE `brand` (
  `brandName` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speciality` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `methodDeliveryBrand` int(10) UNSIGNED NOT NULL,
  `methodPaymentBrand` int(10) UNSIGNED NOT NULL,
  `merchant` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `socialmedia_facebook` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `socialmedia_twitter` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `socialmedia_instagram` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_brand` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `idCategory` int(10) UNSIGNED NOT NULL,
  `subCategory` int(10) UNSIGNED NOT NULL,
  `short_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `user_email` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facture_content`
--

CREATE TABLE `facture_content` (
  `Bag_facture` int(10) UNSIGNED NOT NULL,
  `Product_Reference` int(10) UNSIGNED NOT NULL,
  `amount` decimal(9,2) NOT NULL,
  `subtotal` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feedback`
--

CREATE TABLE `feedback` (
  `num_error` int(10) UNSIGNED NOT NULL,
  `userEmail` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_report` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `merchant`
--

CREATE TABLE `merchant` (
  `id_merchant` int(10) UNSIGNED NOT NULL,
  `user_email` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_delivery_brand`
--

CREATE TABLE `method_delivery_brand` (
  `idMethod_delivery_brand` int(10) UNSIGNED NOT NULL,
  `hand_to_hand` tinyint(4) NOT NULL,
  `land_transport` tinyint(4) NOT NULL,
  `air_transport` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_delivery_product`
--

CREATE TABLE `method_delivery_product` (
  `idMethod_delivery_product` int(10) UNSIGNED NOT NULL,
  `hand_to_hand` tinyint(4) NOT NULL,
  `land_transport` tinyint(4) NOT NULL,
  `air_transport` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_payment_brand`
--

CREATE TABLE `method_payment_brand` (
  `idMethod_payment_brand` int(10) UNSIGNED NOT NULL,
  `cash` tinyint(4) NOT NULL,
  `paypal` tinyint(4) NOT NULL,
  `payU` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_payment_product`
--

CREATE TABLE `method_payment_product` (
  `idMethod_payment_product` int(10) UNSIGNED NOT NULL,
  `cash` tinyint(4) NOT NULL,
  `paypal` tinyint(4) NOT NULL,
  `payU` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_06_13_020436_create_user_table', 1),
(2, '2019_06_13_020437_create_role_table', 1),
(3, '2019_06_13_020438_create_customer_table', 1),
(4, '2019_06_13_020439_create_merchant_table', 1),
(5, '2019_06_13_020440_create_feedback_table', 1),
(6, '2019_06_13_020441_create_method_delivery_brand_table', 1),
(7, '2019_06_13_020442_create_method_payment_brand_table', 1),
(8, '2019_06_13_020443_create_brand_table', 1),
(9, '2019_06_13_020444_create_category_table', 1),
(10, '2019_06_13_020445_create_method_payment_product_table', 1),
(11, '2019_06_13_020446_create_method_delivery_product_table', 1),
(12, '2019_06_13_020447_create_product_table', 1),
(13, '2019_06_13_020448_create_popup_table', 1),
(14, '2019_06_13_020449_create_server_electronic_mail_table', 1),
(15, '2019_06_13_020450_create_bag_facture_table', 1),
(16, '2019_06_13_020451_create_facture_content_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `popup`
--

CREATE TABLE `popup` (
  `idPopup` int(10) UNSIGNED NOT NULL,
  `product_Reference` int(10) UNSIGNED NOT NULL,
  `title_prom` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_prom` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_prom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `reference` int(10) UNSIGNED NOT NULL,
  `productName` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Brand_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productCategory` int(10) UNSIGNED NOT NULL,
  `idMethod_payment_product` int(10) UNSIGNED NOT NULL,
  `idMethod_delivery_product` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `idRole` int(10) UNSIGNED NOT NULL,
  `role` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `server_electronic_mail`
--

CREATE TABLE `server_electronic_mail` (
  `idMailServerMail` int(10) UNSIGNED NOT NULL,
  `user_email` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `electronic_address` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `user_email` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(81) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(81) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bag_facture`
--
ALTER TABLE `bag_facture`
  ADD PRIMARY KEY (`idBag_facture`),
  ADD KEY `bag_facture_customer_foreign` (`Customer`);

--
-- Indices de la tabla `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brandName`),
  ADD KEY `brand_methoddeliverybrand_foreign` (`methodDeliveryBrand`),
  ADD KEY `brand_methodpaymentbrand_foreign` (`methodPaymentBrand`),
  ADD KEY `brand_merchant_foreign` (`merchant`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`),
  ADD KEY `category_subcategory_foreign` (`subCategory`);

--
-- Indices de la tabla `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `customer_user_email_foreign` (`user_email`),
  ADD KEY `customer_user_role_foreign` (`user_role`);

--
-- Indices de la tabla `facture_content`
--
ALTER TABLE `facture_content`
  ADD PRIMARY KEY (`Bag_facture`,`Product_Reference`),
  ADD KEY `facture_content_product_reference_foreign` (`Product_Reference`);

--
-- Indices de la tabla `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`num_error`),
  ADD KEY `feedback_useremail_foreign` (`userEmail`);

--
-- Indices de la tabla `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`id_merchant`),
  ADD KEY `merchant_user_email_foreign` (`user_email`),
  ADD KEY `merchant_user_role_foreign` (`user_role`);

--
-- Indices de la tabla `method_delivery_brand`
--
ALTER TABLE `method_delivery_brand`
  ADD PRIMARY KEY (`idMethod_delivery_brand`);

--
-- Indices de la tabla `method_delivery_product`
--
ALTER TABLE `method_delivery_product`
  ADD PRIMARY KEY (`idMethod_delivery_product`);

--
-- Indices de la tabla `method_payment_brand`
--
ALTER TABLE `method_payment_brand`
  ADD PRIMARY KEY (`idMethod_payment_brand`);

--
-- Indices de la tabla `method_payment_product`
--
ALTER TABLE `method_payment_product`
  ADD PRIMARY KEY (`idMethod_payment_product`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `popup`
--
ALTER TABLE `popup`
  ADD PRIMARY KEY (`idPopup`),
  ADD KEY `popup_product_reference_foreign` (`product_Reference`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`reference`),
  ADD KEY `product_brand_name_foreign` (`Brand_name`),
  ADD KEY `product_productcategory_foreign` (`productCategory`),
  ADD KEY `product_idmethod_payment_product_foreign` (`idMethod_payment_product`),
  ADD KEY `product_idmethod_delivery_product_foreign` (`idMethod_delivery_product`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`idRole`);

--
-- Indices de la tabla `server_electronic_mail`
--
ALTER TABLE `server_electronic_mail`
  ADD PRIMARY KEY (`idMailServerMail`),
  ADD KEY `server_electronic_mail_user_email_foreign` (`user_email`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bag_facture`
--
ALTER TABLE `bag_facture`
  MODIFY `idBag_facture` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `idCategory` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `method_delivery_product`
--
ALTER TABLE `method_delivery_product`
  MODIFY `idMethod_delivery_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `method_payment_product`
--
ALTER TABLE `method_payment_product`
  MODIFY `idMethod_payment_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `popup`
--
ALTER TABLE `popup`
  MODIFY `idPopup` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `reference` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `server_electronic_mail`
--
ALTER TABLE `server_electronic_mail`
  MODIFY `idMailServerMail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bag_facture`
--
ALTER TABLE `bag_facture`
  ADD CONSTRAINT `bag_facture_customer_foreign` FOREIGN KEY (`Customer`) REFERENCES `customer` (`id_customer`);

--
-- Filtros para la tabla `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `brand_merchant_foreign` FOREIGN KEY (`merchant`) REFERENCES `merchant` (`id_merchant`),
  ADD CONSTRAINT `brand_methoddeliverybrand_foreign` FOREIGN KEY (`methodDeliveryBrand`) REFERENCES `method_delivery_brand` (`idMethod_delivery_brand`),
  ADD CONSTRAINT `brand_methodpaymentbrand_foreign` FOREIGN KEY (`methodPaymentBrand`) REFERENCES `method_payment_brand` (`idMethod_payment_brand`);

--
-- Filtros para la tabla `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_subcategory_foreign` FOREIGN KEY (`subCategory`) REFERENCES `category` (`idCategory`);

--
-- Filtros para la tabla `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_user_email_foreign` FOREIGN KEY (`user_email`) REFERENCES `user` (`user_email`),
  ADD CONSTRAINT `customer_user_role_foreign` FOREIGN KEY (`user_role`) REFERENCES `role` (`idRole`);

--
-- Filtros para la tabla `facture_content`
--
ALTER TABLE `facture_content`
  ADD CONSTRAINT `facture_content_bag_facture_foreign` FOREIGN KEY (`Bag_facture`) REFERENCES `bag_facture` (`idBag_facture`),
  ADD CONSTRAINT `facture_content_product_reference_foreign` FOREIGN KEY (`Product_Reference`) REFERENCES `product` (`reference`);

--
-- Filtros para la tabla `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_useremail_foreign` FOREIGN KEY (`userEmail`) REFERENCES `user` (`user_email`);

--
-- Filtros para la tabla `merchant`
--
ALTER TABLE `merchant`
  ADD CONSTRAINT `merchant_user_email_foreign` FOREIGN KEY (`user_email`) REFERENCES `user` (`user_email`),
  ADD CONSTRAINT `merchant_user_role_foreign` FOREIGN KEY (`user_role`) REFERENCES `role` (`idRole`);

--
-- Filtros para la tabla `popup`
--
ALTER TABLE `popup`
  ADD CONSTRAINT `popup_product_reference_foreign` FOREIGN KEY (`product_Reference`) REFERENCES `product` (`reference`);

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_brand_name_foreign` FOREIGN KEY (`Brand_name`) REFERENCES `brand` (`brandName`),
  ADD CONSTRAINT `product_idmethod_delivery_product_foreign` FOREIGN KEY (`idMethod_delivery_product`) REFERENCES `method_delivery_product` (`idMethod_delivery_product`),
  ADD CONSTRAINT `product_idmethod_payment_product_foreign` FOREIGN KEY (`idMethod_payment_product`) REFERENCES `method_payment_product` (`idMethod_payment_product`),
  ADD CONSTRAINT `product_productcategory_foreign` FOREIGN KEY (`productCategory`) REFERENCES `category` (`idCategory`);

--
-- Filtros para la tabla `server_electronic_mail`
--
ALTER TABLE `server_electronic_mail`
  ADD CONSTRAINT `server_electronic_mail_user_email_foreign` FOREIGN KEY (`user_email`) REFERENCES `user` (`user_email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

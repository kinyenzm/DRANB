CREATE SCHEMA IF NOT EXISTS db_Dranb DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE db_Dranb;
 

--
-- Estructura para la tabla 'User'
--

CREATE TABLE User (
user_email VARCHAR(99) NOT NULL,
nickname VARCHAR(45) NOT NULL,
password VARCHAR(128) NOT NULL,
name VARCHAR(81) NOT NULL,
lastname VARCHAR(81) NULL,
photo LONGBLOB NULL,
address VARCHAR(150) NULL,
phone VARCHAR(45) NULL,
PRIMARY KEY (user_email)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Role'
--

CREATE TABLE Role (
idRole INT NOT NULL,
role VARCHAR(45) NOT NULL,
PRIMARY KEY (idRole)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Customer'
--

CREATE TABLE Customer (
id_customer INT NOT NULL,
user_email VARCHAR(99) NOT NULL,
idRole INT NOT NULL,
PRIMARY KEY (id_customer),
FOREIGN KEY (user_email) REFERENCES User (user_email),
FOREIGN KEY (idRole) REFERENCES Role (idRole)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Merchant'
--

CREATE TABLE Merchant (
id_merchant INT NOT NULL,
user_email VARCHAR(99) NOT NULL,
idRole INT NOT NULL,
PRIMARY KEY (id_merchant),
FOREIGN KEY (user_email) REFERENCES User (user_email),
FOREIGN KEY (idRole) REFERENCES Role (idRole)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Feedback'
--

CREATE TABLE Feedback (
num_error INT not NULL AUTO_INCREMENT,
userEmail VARCHAR(99) NOT NULL,
description LONGTEXT NOT NULL,
date DATETIME NOT NULL,
type_report VARCHAR(45) NOT NULL,
PRIMARY KEY (num_error),
FOREIGN KEY (userEmail) REFERENCES User (user_email)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Method_delivery_brand'
--

CREATE TABLE Method_delivery_brand (
idMethod_delivery_brand INT NOT NULL,
hand_to_hand TINYINT NOT NULL,
land_transport TINYINT NOT NULL,
air_transport TINYINT NOT NULL,
PRIMARY KEY (idMethod_delivery_brand)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Method_payment_brand'
--

CREATE TABLE Method_payment_brand (
idMethod_payment_brand INT NOT NULL,
cash TINYINT NOT NULL,
paypal TINYINT NOT NULL,
payU TINYINT NOT NULL,
PRIMARY KEY (idMethod_payment_brand)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Brand'
--

CREATE TABLE Brand (
brandName VARCHAR(45) NOT NULL,
speciality VARCHAR(45) NOT NULL,
description MEDIUMTEXT NOT NULL,
methodDeliveryBrand INT NOT NULL,
methodPaymentBrand INT NOT NULL,
merchant INT NOT NULL,
logo LONGBLOB NULL,
socialmedia_facebook VARCHAR(45) NULL,
socialmedia_twitter VARCHAR(45) NULL,
socialmedia_instagram VARCHAR(45) NULL,
contact_brand VARCHAR(45) NULL,
PRIMARY KEY (brandName),
FOREIGN KEY (methodDeliveryBrand) REFERENCES Method_delivery_brand (idMethod_delivery_brand),
FOREIGN KEY (methodPaymentBrand) REFERENCES Method_payment_brand (idMethod_payment_brand),
FOREIGN KEY (merchant) REFERENCES Merchant (id_merchant)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Category'
--

CREATE TABLE Category (
idCategory INT NOT NULL,
subCategory INT NULL,
short_description MEDIUMTEXT NULL,
category_logo LONGBLOB NULL,
PRIMARY KEY (idCategory),
FOREIGN KEY (subCategory) REFERENCES Category (idCategory)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Method_payment_product'
--

CREATE TABLE Method_payment_product (
idMethod_payment_product INT NOT NULL,
cash TINYINT NOT NULL,
paypal TINYINT NOT NULL,
payU TINYINT NOT NULL,
PRIMARY KEY (idMethod_payment_product)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Method_delivery_product'
--

CREATE TABLE Method_delivery_product (
idMethod_delivery_product INT NOT NULL,
hand_to_hand TINYINT NOT NULL,
land_transport TINYINT NOT NULL,
air_transport TINYINT NOT NULL,
PRIMARY KEY (idMethod_delivery_product)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Product'
--

CREATE TABLE Product (
Reference INT NOT NULL,
productName VARCHAR(45) NOT NULL,
short_description MEDIUMTEXT NOT NULL,
price FLOAT NOT NULL,
product_image LONGBLOB NULL,
Brand_name VARCHAR(45) NOT NULL,
productCategory INT NOT NULL,
idMethod_payment_product INT NOT NULL,
idMethod_delivery_product INT NOT NULL,
PRIMARY KEY (Reference),
FOREIGN KEY (Brand_name) REFERENCES Brand (brandName),
FOREIGN KEY (productCategory) REFERENCES Category (idCategory),
FOREIGN KEY (idMethod_payment_product) REFERENCES Method_payment_product (idMethod_payment_product),
FOREIGN KEY (idMethod_delivery_product) REFERENCES Method_delivery_product (idMethod_delivery_product)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Popup'
--

CREATE TABLE Popup (
idPopup INT NOT NULL,
Product_Reference INT NOT NULL,
title_prom VARCHAR(60) NULL,
description_prom MEDIUMTEXT NULL,
image_prom LONGBLOB NULL,
PRIMARY KEY (idPopup),
FOREIGN KEY (Product_Reference) REFERENCES Product (Reference)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Server_electronic_mail'
--

CREATE TABLE Server_electronic_mail (
idMailServerMail INT NOT NULL,
user_email VARCHAR(99) NOT NULL,
subject VARCHAR(45) NOT NULL,
electronic_address VARCHAR(99) NOT NULL,
message LONGTEXT NOT NULL,
PRIMARY KEY (idMailServerMail),
FOREIGN KEY (user_email) REFERENCES User (user_email)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Bag_facture'
--

CREATE TABLE Bag_facture (
idBag_facture INT NOT NULL AUTO_INCREMENT,
idCustomer INT NOT NULL,
date_purchase DATETIME NOT NULL,
tax FLOAT NOT NULL,
total FLOAT NOT NULL,
PRIMARY KEY (idBag_facture),
FOREIGN KEY (idCustomer) REFERENCES Customer (id_customer)
)ENGINE = InnoDB;

--
-- Estructura para la tabla 'Facture_content'
--

CREATE TABLE Facture_content (
Bag_facture INT NOT NULL,
Product_Reference INT NOT NULL,
amount FLOAT NOT NULL,
subtotal FLOAT NOT NULL,
PRIMARY KEY (Bag_facture,Product_Reference),
FOREIGN KEY (Product_Reference) REFERENCES Product (Reference),
FOREIGN KEY (Bag_facture) REFERENCES Bag_facture (idBag_facture)
)ENGINE = InnoDB;
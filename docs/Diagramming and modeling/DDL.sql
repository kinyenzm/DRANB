-- Creado por: Henderson Moreno
-- Tiempo de generación: 19-05-2018 a las 19:34:00
-- Tipo de servidor: MySQL
-- Proyecto: Dranb | Comercio

--
-- Base de datos: 'db_dranb'
--

CREATE SCHEMA IF NOT EXISTS db_Dranb DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE db_Dranb;

--
-- Estructura para la tabla 'User'
--

CREATE TABLE User (
user_email VARCHAR(99) NOT NULL,
nickname VARCHAR(45) NOT NULL,
password VARCHAR(128) NOT NULL,
name VARCHAR(81) NOT NULL,
lastname VARCHAR(81) NULL,
photo LONGBLOB NULL,
address VARCHAR(150) NULL,
phone VARCHAR(45) NULL,
PRIMARY KEY (user_email)
);

--
-- Estructura para la tabla 'Role'
--

CREATE TABLE Role (
id_role INT NOT NULL,
role VARCHAR(45) NOT NULL,
PRIMARY KEY (id_role)
);

--
-- Estructura para la tabla 'Customer'
--

CREATE TABLE Customer (
id_customer INT NOT NULL,
user_email VARCHAR(99) NOT NULL,
id_role INT NOT NULL,
PRIMARY KEY (id_customer),
FOREIGN KEY (user_email) REFERENCES User (user_email),
FOREIGN KEY (id_role) REFERENCES Role (id_role)
);

--
-- Estructura para la tabla 'Merchant'
--

CREATE TABLE Merchant (
id_merchant INT NOT NULL,
user_email VARCHAR(99) NOT NULL,
id_role INT NOT NULL,
PRIMARY KEY (id_merchant),
FOREIGN KEY (user_email) REFERENCES User (user_email),
FOREIGN KEY (id_role) REFERENCES Role (id_role)
);

--
-- Estructura para la tabla 'Feedback'
--

CREATE TABLE Feedback (
num_error INT NOT NULL AUTO_INCREMENT,
user_email VARCHAR(99) NOT NULL,
description LONGTEXT NOT NULL,
cur_date DATETIME NOT NULL,
type_report VARCHAR(45) NOT NULL,
PRIMARY KEY (num_error),
FOREIGN KEY (user_email) REFERENCES User (user_email)
);

--
-- Estructura para la tabla 'Method_delivery_brand'
--

CREATE TABLE Method_delivery_brand (
id_method_delivery_brand INT NOT NULL,
hand_to_hand TINYINT NOT NULL,
land_transport TINYINT NOT NULL,
air_transport TINYINT NOT NULL,
PRIMARY KEY (id_method_delivery_brand)
);

--
-- Estructura para la tabla 'Method_payment_brand'
--

CREATE TABLE Method_payment_brand (
id_method_payment_brand INT NOT NULL,
cash TINYINT NOT NULL,
paypal TINYINT NOT NULL,
payU TINYINT NOT NULL,
PRIMARY KEY (id_method_payment_brand)
);

--
-- Estructura para la tabla 'Brand'
--

CREATE TABLE Brand (
brand_name VARCHAR(45) NOT NULL,
speciality VARCHAR(45) NOT NULL,
description MEDIUMTEXT NOT NULL,
method_delivery_brand INT NOT NULL,
method_payment_brand INT NOT NULL,
merchant INT NOT NULL,
logo LONGBLOB NULL,
socialmedia_facebook VARCHAR(45) NULL,
socialmedia_twitter VARCHAR(45) NULL,
socialmedia_instagram VARCHAR(45) NULL,
contact_brand VARCHAR(45) NULL,
PRIMARY KEY (brand_name),
FOREIGN KEY (method_delivery_brand) REFERENCES Method_delivery_brand (id_method_delivery_brand),
FOREIGN KEY (method_payment_brand) REFERENCES Method_payment_brand (id_method_payment_brand),
FOREIGN KEY (merchant) REFERENCES Merchant (id_merchant)
);

--
-- Estructura para la tabla 'Category'
--

CREATE TABLE Category (
id_category INT NOT NULL,
sub_category INT NULL,
short_description MEDIUMTEXT NULL,
category_logo LONGBLOB NULL,
PRIMARY KEY (id_category),
FOREIGN KEY (sub_category) REFERENCES Category (id_category)
);

--
-- Estructura para la tabla 'Method_payment_product'
--

CREATE TABLE Method_payment_product (
id_method_payment_product INT NOT NULL,
cash TINYINT NOT NULL,
paypal TINYINT NOT NULL,
payu TINYINT NOT NULL,
PRIMARY KEY (id_method_payment_product)
);

--
-- Estructura para la tabla 'Method_delivery_product'
--

CREATE TABLE Method_delivery_product (
id_method_delivery_product INT NOT NULL,
hand_to_hand TINYINT NOT NULL,
land_transport TINYINT NOT NULL,
air_transport TINYINT NOT NULL,
PRIMARY KEY (id_method_delivery_product)
);

--
-- Estructura para la tabla 'Product'
--

CREATE TABLE Product (
reference INT NOT NULL,
product_name VARCHAR(45) NOT NULL,
short_description MEDIUMTEXT NOT NULL,
price FLOAT NOT NULL,
product_image LONGBLOB NULL,
brand_name VARCHAR(45) NOT NULL,
product_category INT NOT NULL,
id_method_payment_product INT NOT NULL,
id_method_delivery_product INT NOT NULL,
PRIMARY KEY (reference),
FOREIGN KEY (brand_name) REFERENCES Brand (brand_name),
FOREIGN KEY (product_category) REFERENCES Category (id_category),
FOREIGN KEY (id_method_payment_product) REFERENCES Method_payment_product (id_method_payment_product),
FOREIGN KEY (id_method_delivery_product) REFERENCES Method_delivery_product (id_method_delivery_product)
);

--
-- Estructura para la tabla 'Popup'
--

CREATE TABLE Popup (
id_popup INT NOT NULL,
product_reference INT NOT NULL,
title_prom VARCHAR(60) NULL,
description_prom MEDIUMTEXT NULL,
image_prom LONGBLOB NULL,
PRIMARY KEY (id_popup),
FOREIGN KEY (product_reference) REFERENCES Product (reference)
);

--
-- Estructura para la tabla 'Server_electronic_mail'
--

CREATE TABLE Server_electronic_mail (
id_mail_server_mail INT NOT NULL,
user_email VARCHAR(99) NOT NULL,
subject VARCHAR(45) NOT NULL,
message LONGTEXT NOT NULL,
date_mail DATETIME NOT NULL,
PRIMARY KEY (id_mail_server_mail),
FOREIGN KEY (user_email) REFERENCES User (user_email)
);

--
-- Estructura para la tabla 'Bag_facture'
--

CREATE TABLE Bag_facture (
id_bag_facture INT NOT NULL AUTO_INCREMENT,
id_customer INT NOT NULL,
date_purchase DATETIME NOT NULL,
tax FLOAT NOT NULL,
total FLOAT NOT NULL,
PRIMARY KEY (id_bag_facture),
FOREIGN KEY (id_customer) REFERENCES Customer (id_customer)
);

--
-- Estructura para la tabla 'Facture_content'
--

CREATE TABLE Facture_content (
bag_facture INT NOT NULL,
product_reference INT NOT NULL,
amount FLOAT NOT NULL,
subtotal FLOAT NOT NULL,
PRIMARY KEY (bag_facture,product_reference),
FOREIGN KEY (product_reference) REFERENCES Product (reference),
FOREIGN KEY (bag_facture) REFERENCES Bag_facture (id_bag_facture)
);

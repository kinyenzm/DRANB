-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-11-2019 a las 02:06:57
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;

--
-- Base de datos: `db_dranb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bag_facture`
--

DROP TABLE IF EXISTS `bag_facture`;
CREATE TABLE IF NOT EXISTS `bag_facture` (
  `id_bag_facture` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `date_purchase` datetime NOT NULL,
  `tax` float NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_bag_facture`),
  KEY `id_customer` (`id_customer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brand`
--

DROP TABLE IF EXISTS `brand`;
CREATE TABLE IF NOT EXISTS `brand` (
  `brand_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `speciality` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `method_delivery_brand` int(11) NOT NULL,
  `method_payment_brand` int(11) NOT NULL,
  `merchant` int(11) NOT NULL,
  `logo` longblob,
  `socialmedia_facebook` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socialmedia_twitter` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socialmedia_instagram` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_brand` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`brand_name`),
  KEY `method_delivery_brand` (`method_delivery_brand`),
  KEY `method_payment_brand` (`method_payment_brand`),
  KEY `merchant` (`merchant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL,
  `sub_category` int(11) DEFAULT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci,
  `category_logo` longblob,
  PRIMARY KEY (`id_category`),
  KEY `sub_category` (`sub_category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` int(11) NOT NULL,
  `user_email` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `id_role` int(11) NOT NULL,
  PRIMARY KEY (`id_customer`),
  KEY `user_email` (`user_email`),
  KEY `id_role` (`id_role`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facture_content`
--

DROP TABLE IF EXISTS `facture_content`;
CREATE TABLE IF NOT EXISTS `facture_content` (
  `bag_facture` int(11) NOT NULL,
  `product_reference` int(11) NOT NULL,
  `amount` float NOT NULL,
  `subtotal` float NOT NULL,
  PRIMARY KEY (`bag_facture`,`product_reference`),
  KEY `product_reference` (`product_reference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `num_error` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cur_date` datetime NOT NULL,
  `type_report` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`num_error`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `merchant`
--

DROP TABLE IF EXISTS `merchant`;
CREATE TABLE IF NOT EXISTS `merchant` (
  `id_merchant` int(11) NOT NULL,
  `user_email` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `id_role` int(11) NOT NULL,
  PRIMARY KEY (`id_merchant`),
  KEY `user_email` (`user_email`),
  KEY `id_role` (`id_role`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_delivery_brand`
--

DROP TABLE IF EXISTS `method_delivery_brand`;
CREATE TABLE IF NOT EXISTS `method_delivery_brand` (
  `id_method_delivery_brand` int(11) NOT NULL,
  `hand_to_hand` tinyint(4) NOT NULL,
  `land_transport` tinyint(4) NOT NULL,
  `air_transport` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_method_delivery_brand`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_delivery_product`
--

DROP TABLE IF EXISTS `method_delivery_product`;
CREATE TABLE IF NOT EXISTS `method_delivery_product` (
  `id_method_delivery_product` int(11) NOT NULL,
  `hand_to_hand` tinyint(4) NOT NULL,
  `land_transport` tinyint(4) NOT NULL,
  `air_transport` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_method_delivery_product`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_payment_brand`
--

DROP TABLE IF EXISTS `method_payment_brand`;
CREATE TABLE IF NOT EXISTS `method_payment_brand` (
  `id_method_payment_brand` int(11) NOT NULL,
  `cash` tinyint(4) NOT NULL,
  `paypal` tinyint(4) NOT NULL,
  `payU` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_method_payment_brand`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `method_payment_product`
--

DROP TABLE IF EXISTS `method_payment_product`;
CREATE TABLE IF NOT EXISTS `method_payment_product` (
  `id_method_payment_product` int(11) NOT NULL,
  `cash` tinyint(4) NOT NULL,
  `paypal` tinyint(4) NOT NULL,
  `payu` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_method_payment_product`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `popup`
--

DROP TABLE IF EXISTS `popup`;
CREATE TABLE IF NOT EXISTS `popup` (
  `id_popup` int(11) NOT NULL,
  `product_reference` int(11) NOT NULL,
  `title_prom` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_prom` mediumtext COLLATE utf8_unicode_ci,
  `image_prom` longblob,
  PRIMARY KEY (`id_popup`),
  KEY `product_reference` (`product_reference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `reference` int(11) NOT NULL,
  `product_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `product_image` longblob,
  `brand_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `product_category` int(11) NOT NULL,
  `id_method_payment_product` int(11) NOT NULL,
  `id_method_delivery_product` int(11) NOT NULL,
  PRIMARY KEY (`reference`),
  KEY `brand_name` (`brand_name`),
  KEY `product_category` (`product_category`),
  KEY `id_method_payment_product` (`id_method_payment_product`),
  KEY `id_method_delivery_product` (`id_method_delivery_product`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(11) NOT NULL,
  `role` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `server_electronic_mail`
--

DROP TABLE IF EXISTS `server_electronic_mail`;
CREATE TABLE IF NOT EXISTS `server_electronic_mail` (
  `id_mail_server_mail` int(11) NOT NULL,
  `user_email` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_mail` datetime NOT NULL,
  PRIMARY KEY (`id_mail_server_mail`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_email` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(81) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(81) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` longblob,
  `address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
